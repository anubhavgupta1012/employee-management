package com.employee.management.config;

public class RoleConstants {

	public static final String SUCCESS = "success";
	public static final String ROLE_ADMIN = "ROLE_ADMIN";
	public static final String ROLE_USER = "USER";

}
