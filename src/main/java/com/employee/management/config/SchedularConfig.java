package com.employee.management.config;

import com.employee.management.service.UserService;
import com.employee.management.service.impl.UserServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class SchedularConfig {

	private static Logger logger = LoggerFactory.getLogger(SchedularConfig.class);
	
    @Autowired
    private UserService userService;

    @Scheduled(cron = "0 0 12 * * ?")
    public void validateUser() {
    	logger.info("**checking to validate User **");
        userService.validateUsers();
    }
}
