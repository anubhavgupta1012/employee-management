
package com.employee.management.config;

import java.io.IOException;
import java.util.Base64;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;



@Component
@Order(-1000)
public class RequestFilter implements javax.servlet.Filter {

	public RequestFilter() {
		super();
	}
	

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain)
			throws IOException, ServletException {

		MyServletRequestWrapper httpReq = new MyServletRequestWrapper((HttpServletRequest) req);
		HttpServletResponse httpRes = (HttpServletResponse) res;
		
		if ("/oauth/token".equalsIgnoreCase(httpReq.getRequestURI())) {
			byte[] encoded = Base64.getEncoder().encode("employee-client:employee-client-secret".getBytes("UTF-8"));
			httpReq.addHeader("Authorization", "Basic " + new String(encoded));
		}

		filterChain.doFilter(httpReq, httpRes);

	}

	
	public void destroy() {
		
	}
	
}