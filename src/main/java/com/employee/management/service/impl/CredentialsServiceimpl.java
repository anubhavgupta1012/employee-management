package com.employee.management.service.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.employee.management.model.Credentials;
import com.employee.management.repository.CredentialsRepository;
import com.employee.management.service.CredentialsService;

@Service("credentialService")
public class CredentialsServiceimpl implements CredentialsService, UserDetailsService {

	@Autowired
	CredentialsRepository credentialsRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Credentials credentials = credentialsRepository.findByuserName(username);

		Set<GrantedAuthority> grantedAuthorities = getAuthorities(credentials);

	
		return new org.springframework.security.core.userdetails.User(credentials.getUserName(),
				(credentials.getPassword()), grantedAuthorities);
	}

	private Set<GrantedAuthority> getAuthorities(Credentials credentials) {
		Set<GrantedAuthority> authorities = new HashSet<>();
		authorities.add(new SimpleGrantedAuthority("ROLE_"+credentials.getRole().toUpperCase()));
		return authorities;
	}

}
