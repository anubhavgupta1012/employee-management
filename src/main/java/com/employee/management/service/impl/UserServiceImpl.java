package com.employee.management.service.impl;

import com.employee.management.model.Employee;
import com.employee.management.model.User;
import com.employee.management.repository.EmployeeRepository;
import com.employee.management.repository.UserRepository;
import com.employee.management.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EmployeeRepository employeeRepository;


    @Override
    public boolean submitUserData(User user) {
        User save = userRepository.save(user);
        return save != null;
    }

    @Override
    public void validateUsers() {
        List<User> allUsers = userRepository.findAll();
        List<Employee> createdEmployees = allUsers.stream()
            .filter(user -> (user.isInterviewPassed() && Integer.parseInt(user.getAge()) >= 18))
            .map(user -> addValidUsersToEmployee(user)).collect(Collectors.toList());
    }

    private Employee addValidUsersToEmployee(User user) {
        Employee employee = new Employee();
        employee.setDob(user.getDob());
        employee.setEmail(user.getEmail());
        employee.setName(user.getName());
        employee.setJoiningDate(user.getJoiningDate());
        employee.setLocation(user.getLocation());
        Employee savedEmployee = employeeRepository.save(employee);
        logger.info("user :{} saved to EMPLOYEE Table", savedEmployee.getName());
        userRepository.delete(user);
        logger.info("user :{} deleted from USER Table", user.getName());
        return savedEmployee;
    }
}
