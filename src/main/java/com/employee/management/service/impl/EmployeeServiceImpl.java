package com.employee.management.service.impl;

import com.employee.management.model.Employee;
import com.employee.management.repository.EmployeeRepository;
import com.employee.management.service.EmployeeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee getEmployeeById(Long id) {
        return employeeRepository.findById(id).get();
    }

    @Override
    public Long submittedEmployee(Employee employee) {
        Employee savedEmployee = employeeRepository.save(employee);
        return savedEmployee.getEmpId();
    }

    @Override
    public Long deleteEmployee(Long id) {
        /*TO-DO
         *Keep a flag for active/ Non-active Employees
         */
        employeeRepository.deleteById(id);
        return id;
    }

    @Override
    public Long updateEmployee(Long id, Employee employee) {
        if (employee != null) {
            employee.setEmpId(id);
            Employee employeeById = employeeRepository.getById(id);
//            employeeById.setLocation(employee.getLocation())
//                .setDob(employee.getDob())  // new SimpleDateFormat("dd/MM/yyyy").parse(employee.getDob())
//                .setName(employee.getName())
//                .setJoiningDate(employee.getJoiningDate())
//                .setEmail(employee.getEmail());

            BeanUtils.copyProperties(employee, employeeById);
            employeeRepository.save(employeeById);
            return employeeById.getEmpId();
        }
        return null;
    }
}
