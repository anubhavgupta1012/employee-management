package com.employee.management.service;

import com.employee.management.model.User;

public interface UserService {

    boolean submitUserData(User user);

    void validateUsers();
}
