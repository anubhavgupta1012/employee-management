package com.employee.management.service;

import com.employee.management.model.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> getAllEmployees();

    Employee getEmployeeById(Long id);

    Long submittedEmployee(Employee employee);

    Long deleteEmployee(Long id);

    Long updateEmployee(Long id, Employee employee);
}
