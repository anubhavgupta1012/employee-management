package com.employee.management.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Assignment {

    @EmbeddedId
    private AssignmentId assignmentId;
    private String startDate;
    private String endDate;
    private Long managerId;

    @ManyToOne
    private Employee employee;

    @ManyToOne
    private Project project;

    public Assignment(){}

    public Assignment(AssignmentId assignmentId,
                      String startDate, String endDate,
                      Long managerId, Employee employee, Project project) {
        this.assignmentId = assignmentId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.managerId = managerId;
        this.employee = employee;
        this.project = project;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public AssignmentId getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(AssignmentId assignmentId) {
        this.assignmentId = assignmentId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }
}
