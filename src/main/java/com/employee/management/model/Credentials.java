package com.employee.management.model;

//    ^ represents starting character of the string.
//    (?=.*[0-9]) represents a digit must occur at least once.
//    (?=.*[a-z]) represents a lower case alphabet must occur at least once.
//    (?=.*[A-Z]) represents an upper case alphabet that must occur at least once.
//    (?=.*[@#$%^&-+=()] represents a special character that must occur at least once.
//    (?=\\S+$) white spaces don’t allowed in the entire string.
//    .{8, 20} represents at least 8 characters and at most 20 characters.
//    $ represents the end of the string.
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Credentials {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long userId;
	private String userName;
	private String password;
    private String role;
    private String email;
    
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}



}
