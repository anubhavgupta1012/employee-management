package com.employee.management.model;

import javax.persistence.*;

@Entity
public class Payroll {

    @Id
    private Long payrollId;
    private Double salary;
    private String accNo;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "empId", nullable = false, unique = true)
    private Employee employee;

    public Payroll(){}

    public Payroll(Double salary, String accNo, Employee employee) {
        this.salary = salary;
        this.accNo = accNo;
        this.employee = employee;
    }

    public Long getPayrollId() {
        return payrollId;
    }

    public void setPayrollId(Long payrollId) {
        this.payrollId = payrollId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }
}
