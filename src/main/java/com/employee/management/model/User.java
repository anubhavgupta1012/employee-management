package com.employee.management.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class User {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String userId;
    private String name;
    private String email;
    private String dob;
    private String location;
    private String joiningDate;
    private String age;
    private boolean interviewPassed;

    public String getUserId() {
        return userId;
    }

    public User setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getJoiningDate() {
        return joiningDate;
    }

    public User setJoiningDate(String joiningDate) {
        this.joiningDate = joiningDate;
        return this;
    }

    public String getLocation() {
        return location;
    }

    public User setLocation(String location) {
        this.location = location;
        return this;
    }

    public String getAge() {
        return age;
    }

    public User setAge(String age) {
        this.age = age;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public boolean isInterviewPassed() {
        return interviewPassed;
    }

    public User setInterviewPassed(boolean interviewPassed) {
        this.interviewPassed = interviewPassed;
        return this;
    }

    public String getDob() {
        return dob;
    }

    public User setDob(String dob) {
        this.dob = dob;
        return this;
    }
}
