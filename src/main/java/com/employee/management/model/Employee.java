package com.employee.management.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long empId;
	private String name;
	private String gender;
	private String dob;
	private String location;
	private String joiningDate;
	private String position;
	private String role;

	@Column(unique = true)
	private String email;
	private String phoneNo;
//
//    @OneToMany(mappedBy = "employee", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    private List<Assignment> assignment;
//
//    @OneToOne(mappedBy = "employee")
//    private Payroll payroll;
//
//    @OneToOne(mappedBy = "employee")
//    private Credentials credentials;

	public Employee() {
	}

	public Employee(String name, String gender, String dob, String location, String joiningDate, String position,
			String role, String email, String phoneNo) {
		this.name = name;
		this.gender = gender;
		this.dob = dob;
		this.location = location;
		this.joiningDate = joiningDate;
		this.position = position;
		this.role = role;
		this.email = email;
		this.phoneNo = phoneNo;
//        this.assignment = assignment;
//        this.payroll = payroll;
//        this.credentials = credentials;
	}

//    public List<Assignment> getAssignment() {
//        return assignment;
//    }
//
//    public void setAssignment(List<Assignment> assignment) {
//        this.assignment = assignment;
//    }
//
//    public Payroll getPayroll() {
//        return payroll;
//    }
//
//    public void setPayroll(Payroll payroll) {
//        this.payroll = payroll;
//    }
//
//    public Credentials getCredentials() {
//        return credentials;
//    }
//
//    public void setCredentials(Credentials credentials) {
//        this.credentials = credentials;
//    }

	public Long getEmpId() {
		return empId;
	}

	public void setEmpId(Long empId) {
		this.empId = empId;
	}

	public String getName() {
		return name;
	}

	public Employee setName(String name) {
		this.name = name;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getJoiningDate() {
		return joiningDate;
	}

	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
