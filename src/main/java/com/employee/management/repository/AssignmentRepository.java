package com.employee.management.repository;

import com.employee.management.model.Assignment;
import com.employee.management.model.AssignmentId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssignmentRepository extends JpaRepository<Assignment, AssignmentId> {
}
