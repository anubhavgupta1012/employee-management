package com.employee.management.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.employee.management.model.Credentials;

public interface CredentialsRepository extends JpaRepository<Credentials, Long> {

	
	Credentials findByuserName(String name);
	
}
