package com.employee.management.controller;

import com.employee.management.model.User;
import com.employee.management.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/userapi")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/user")
    public boolean submitRequest(@RequestBody User user) {
        return userService.submitUserData(user);
    }
}
