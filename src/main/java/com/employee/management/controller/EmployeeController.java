package com.employee.management.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.management.config.RoleConstants;
import com.employee.management.model.Employee;
import com.employee.management.service.EmployeeService;

@RestController
@RequestMapping("/api/v1")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

   
    @Secured(RoleConstants.ROLE_ADMIN)
    @GetMapping("/employees")
    public ResponseEntity<List<Employee>> getAllEmployees() {
    	 return new ResponseEntity<>(employeeService.getAllEmployees(), HttpStatus.OK);
    }

    
    @GetMapping("/employees/{id}")
    public ResponseEntity<Employee> getAllEmployee(@PathVariable("id") String id) {
        return new ResponseEntity<>(employeeService.getEmployeeById(Long.parseLong(id)), HttpStatus.OK);
    }

    @Secured(RoleConstants.ROLE_ADMIN)
    @PostMapping("/employees")
    public ResponseEntity<Long> submitEmployeeRequest(@RequestBody Employee employee) {
        return new ResponseEntity<Long>(employeeService.submittedEmployee(employee), HttpStatus.CREATED);
    }

    @Secured(RoleConstants.ROLE_ADMIN)
    @DeleteMapping("/employees/{id}")
    public ResponseEntity<Long> deleteEmployee(@PathVariable("id") String id) {
        return new ResponseEntity<Long>(employeeService.deleteEmployee(Long.parseLong(id)), HttpStatus.OK);
    }

    @Secured(RoleConstants.ROLE_ADMIN)
    @PutMapping("/employees/{id}")
    public ResponseEntity<Long> updateEmployee(@PathVariable("id") String id, @RequestBody Employee employee) {
        return new ResponseEntity<Long>(employeeService.updateEmployee(Long.parseLong(id), employee), HttpStatus.OK);
    }
}
